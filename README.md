## DESAFIO 1
### Objetivo
Dar desafios práticos para habituar vocês ao Linux.

### Instruções do Desafio
1. Vocês devem criar um repositório com o nome: desafio-01-linux
2. Nesse repositório, vocês devem subir um arquivo markdown e todas as respostas devem estar nele. (https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
3. Para cada etapa, vocês devem adicionar o comando que vocês utilizaram e a saída do resultado obtido.

### Desafio
Passo 1 - Crie uma pasta chamada desafio-01-linux via linha de comando

```sh
mkdir desafio-01-linux
$
```

Passo 2 - Crie um arquivo vazio chamado arquivo desafio-01-linux.txt via linha de comando

```sh
touch desafio-01-linux.txt
$
```

Passo 3 - Adicione a palavra “fee” no arquivo via linha de comando

```sh
cat > desafio-01-linux.txt
$
```

Passo 4 - Visualize o conteúdo do arquivo criado via linha de comando

```sh
cat desafio-01-linux.txt
fee
```

Passo 5 - Crie uma nova pasta chamada sub-pasta-desafio-01 via linha de comando

```sh
mkdir sub-pasta-desafio-01
$
```

Passo 6 - Mova o arquivo desafio-01-linux.txt para a nova pasta criada via linha de comando

```sh
mv desafio-01-linux.txt ~/desafio-01-linux/sub-pasta-desafio-01
$
```

Passo 7 - Renomeie o arquivo para df-01-linux.txt via linha de comando

```sh
mv desafio-01-linux.txt df-01-linux.txt
$
```

Passo 8 - Volte a pasta raiz e delete as pastas e arquivo criados via linha de comando

```sh
rm -r desafio-01-linux
$
```

Passo 9 - Instale o pacote “vim” via linha de comando

```sh
sudo apt-get install vim
```

Passo 10 - Descubra o nome da sua maquina via linha de comando

```sh
hostname
vinicius-skyone
```

Passo 11 - Descubra o IP da sua maquina via linha de comando

```sh
ip r
default via 172.20.10.1 dev wlp2s0 proto dhcp metric 600 
169.254.0.0/16 dev wlp2s0 scope link metric 1000 
172.20.10.0/28 dev wlp2s0 proto kernel scope link src 172.20.10.2 metric 600 
```
